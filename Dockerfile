FROM ubuntu:14.04
MAINTAINER Mark Fletcher

#install dnsmasq
RUN echo 29-09-2014
RUN DEBIAN_FRONTEND=noninteractive apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y dnsmasq

CMD ["dnsmasq", "-k"]
